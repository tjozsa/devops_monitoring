### Grafana + Prometheus + Loki + Jaeger
Open source correlation of Metrics, Logs and Traces in one tool called Grafana.

```bash
$ docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
```
more information here:
https://grafana.com/docs/loki/latest/clients/docker-driver/
https://grafana.com/docs/loki/latest/clients/docker-driver/configuration/

In order to start the monitoring solution you need to
```bash
$ cd grafana_prometheus_loki_jaeger
$ docker-compose up -d
```

As this solution will use Docker Loki Logger plugin you will not see any logs in the syslog, nor in docker console library, and not in systemout.

Before you start the test application make sure that you update the backend API url in the configuration file:

```bash
cd movies-app
cd client
nano .env
# change REACT_APP_API_BASE_URL=http://23.97.212.110:3000 if you are running on your own host
# change REACT_APP_API_BASE_URL=http://ip172-18-0-7-btvmkp2g770000bolcp0-3000.direct.labs.play-with-docker.com if you are running on docker sandbox
```


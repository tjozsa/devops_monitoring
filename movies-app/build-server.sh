#!/bin/bash

echo '\n\n******building server docker image******\n\n'
echo '\n\n******cleaning up database content******\n\n'
sudo rm -Rf ./server/data/
echo '\n\n******starting server docker build******\n\n'
docker-compose build --no-cache server
echo '\n\n******completed docker build******\n\n'
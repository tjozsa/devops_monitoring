#!/bin/bash

echo '\n\n******building client docker image******\n\n'
echo '\n\n******starting client docker build******\n\n'
docker-compose build --no-cache client
echo '\n\n******completed docker build******\n\n'
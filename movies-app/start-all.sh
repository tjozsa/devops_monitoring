#!/bin/bash

echo '\n\n****** starting all services ******\n\n'
docker-compose up -d
docker-compose ps
echo '\n\n****** all services should be running ******\n\n'
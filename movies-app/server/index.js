const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const jaegerHost = process.env.JAEGER_HOST || 'jaeger';
const logger = require('pino')()
// Import and initialize the tracer
const tracer = require('./tracer')('movies', jaegerHost, logger);

const db = require('./db')
const movieRouter = require('./routes/movie-router')

const expressPino = require('express-pino-logger')({
  logger: logger.child({"service": "httpd"})
})

const app = express()
const apiPort = 3000

app.use(expressPino)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.use('/api', movieRouter)

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))


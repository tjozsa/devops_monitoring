#!/bin/bash

echo '\n\n****** stopping all services ******\n\n'
docker-compose down
docker-compose ps
echo '\n\n****** all services should be stoped ******\n\n'